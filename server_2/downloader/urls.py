from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'downloader'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='start/', permanent = False), name='$'),
    url(r'start/', start, name='start'),
]