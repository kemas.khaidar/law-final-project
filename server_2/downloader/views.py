from background_task import background
from django.http import JsonResponse
from .models import Files

import os, pika, requests, sys

def start(request):
    startConsuming()
    return JsonResponse({"start": "starting"})

@background()
def startConsuming():
    print("start consuming")
    routing_key = "routingabcd12345"
    credentials = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters("152.118.148.95", 5672, "/0806444524", credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()    
    channel.exchange_declare(exchange="1606831035_DIRECT", exchange_type="direct")
    result = channel.queue_declare(queue="")
    queue_name = result.method.queue
    channel.queue_bind(exchange="1606831035_DIRECT", queue=queue_name, routing_key=routing_key)
    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()

def callback(ch, method, properties, body):
    message = body.decode("utf-8")
    seq = message.split()[0]
    url = message.split()[-1]
    print("received: " + message)
    file_name = "file_" + str(seq) + ":" + url.split("/")[-1]
    new_file = Files(no=seq, name=file_name)
    new_file.save()
    with open(file_name, "wb") as f:
        response = requests.get(url, stream=True)
        total_length = response.headers.get('content-length')
        if total_length is None:
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sendMessage(seq + " " + str(done*2))
    if (seq == "10"):
        sendFiles()

def sendMessage(message):
    credentials = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters("152.118.148.95", 5672, "/0806444524", credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1606831035_TOPIC", exchange_type="topic")
    channel.basic_publish(exchange="1606831035_TOPIC", routing_key="download", body=message)
    print(" [x] Sent %r" % (message))
    connection.close()

def sendFiles():
    filesDownloaded = Files.objects.all()
    files = {}
    for file in filesDownloaded:
        files["file" + str(file.no)] = open(file.name,'rb')
    url = 'http://localhost:8003/compress/'
    r = requests.post(url, files=files)
    print("sent files to server 3")
    cleanup()

def cleanup():
    filesDownloaded = Files.objects.all()
    for file in filesDownloaded:
        os.remove(file.name)
        print("removing " + file.name)
    Files.objects.all().delete()
