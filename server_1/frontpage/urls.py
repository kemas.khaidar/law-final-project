from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'frontpage'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='index/', permanent = False), name='$'),
    url(r'index/', index, name='index'),
    url(r'progress/', progress, name='progress'),
]
