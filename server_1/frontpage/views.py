from django.shortcuts import render

import pika

def index(request):
    response = {}
    return render(request, "home.html", response)

def progress(request):
    response = {}
    if request.POST:
        url1 = request.POST["url1"]
        set1 = {"url": url1, "download":"download1"}
        sendMessage(1, url1)
        url2 = request.POST["url2"]
        set2 = {"url": url2, "download":"download2"}
        sendMessage(2, url2)
        url3 = request.POST["url3"]
        set3 = {"url": url3, "download":"download3"}
        sendMessage(3, url3)
        url4 = request.POST["url4"]
        set4 = {"url": url4, "download":"download4"}
        sendMessage(4, url4)
        url5 = request.POST["url5"]
        set5 = {"url": url5, "download":"download5"}
        sendMessage(5, url5)
        url6 = request.POST["url6"]
        set6 = {"url": url6, "download":"download6"}
        sendMessage(6, url6)
        url7 = request.POST["url7"]
        set7 = {"url": url7, "download":"download7"}
        sendMessage(7, url7)
        url8 = request.POST["url8"]
        set8 = {"url": url8, "download":"download8"}
        sendMessage(8, url8)
        url9 = request.POST["url9"]
        set9 = {"url": url1, "download":"download9"}
        sendMessage(9, url9)
        url10 = request.POST["url10"]
        set10 = {"url": url10, "download":"download10"}
        sendMessage(10, url10)
        response["urls"] = [set1, set2, set3, set4, set5, set6, set7, set8, set9, set10]
    return render(request, "progress.html", response)

def sendMessage(seq, message):
    routing_key = "routingabcd12345"
    seq_message = str(seq) + " " + message
    credentials = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters("152.118.148.95", 5672, "/0806444524", credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1606831035_DIRECT", exchange_type="direct")
    channel.basic_publish(exchange="1606831035_DIRECT", routing_key=routing_key, body=seq_message)
    print(" [x] Sent %r:%r" % (seq, message))
    connection.close()