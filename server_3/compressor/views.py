from background_task import background
from django.http import JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt

import os, pika, requests, zipfile, base64, hashlib, calendar, datetime

@csrf_exempt
def compress(request):
    response = {}
    try:
        fileNames = []
        file1 = request.FILES['file1']
        fileNames.append(file1.name)
        handle_uploaded_file(file1)
        file2 = request.FILES['file2']
        fileNames.append(file2.name)
        handle_uploaded_file(file2)
        file3 = request.FILES['file3']
        fileNames.append(file3.name)
        handle_uploaded_file(file3)
        file4 = request.FILES['file4']
        fileNames.append(file4.name)
        handle_uploaded_file(file4)
        file5 = request.FILES['file5']
        fileNames.append(file5.name)
        handle_uploaded_file(file5)
        file6 = request.FILES['file6']
        fileNames.append(file6.name)
        handle_uploaded_file(file6)
        file7 = request.FILES['file7']
        fileNames.append(file7.name)
        handle_uploaded_file(file7)
        file8 = request.FILES['file8']
        fileNames.append(file8.name)
        handle_uploaded_file(file8)
        file9 = request.FILES['file9']
        fileNames.append(file9.name)
        handle_uploaded_file(file9)
        file10 = request.FILES['file10']
        fileNames.append(file10.name)
        handle_uploaded_file(file10)
        compressFileRequest(fileNames)
        response['success'] = "True"
        return JsonResponse(response)
    except Exception as e:
        response['success'] = "False"
    finally:
        return JsonResponse(response)

def handle_uploaded_file(f):
    with open(f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

@background()
def compressFileRequest(fileNames):
    out_name = "compression_result.tar.gz"
    my_zipfile = zipfile.ZipFile(out_name, mode='w', compression=zipfile.ZIP_DEFLATED)
    sendMessage("0", "compression")
    print("start compression")
    for i in range(10):
        my_zipfile.write(fileNames[i])
        sendMessage(str((i+1)*10), "compression")
        os.remove(fileNames[i])
    my_zipfile.close()
    sendMessage("Preparing download link...", "link")
    sendFile()

def sendFile():
    url = 'http://localhost:8004/upload/'
    file_name = "compression_result.tar.gz"
    file = {"file": open(file_name,'rb')}
    r = requests.post(url, files=file)
    print("compressed file sent")
    sendEncryptedUrl()

def sendMessage(message, topic):
    credentials = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters("152.118.148.95", 5672, "/0806444524", credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1606831035_TOPIC", exchange_type="topic")
    channel.basic_publish(exchange="1606831035_TOPIC", routing_key=topic, body=message)
    print(" [x] Sent %r" % (message))
    connection.close()

def download(request):
    file_name = "compression_result.tar.gz"
    file = open(file_name,'rb')
    return FileResponse(file)

def sendEncryptedUrl():
    secret = "secretKEMAS"
    url = "/download/"

    future = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
    expiry = calendar.timegm(future.timetuple())

    secure_link = "{key}{url}{expiry}".format(key=secret,
                                              url=url,
                                              expiry=expiry)
    hashed = hashlib.md5(secure_link.encode("utf-8")).digest()
    encoded_hash = base64.urlsafe_b64encode(hashed).rstrip(b"=")
    urlEncrypted = "http://localhost:8004/download/" + "?st=" + encoded_hash.decode("utf-8") + "&e=" + str(expiry)
    sendMessage(urlEncrypted, "link")