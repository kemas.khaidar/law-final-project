from background_task import background
from django.http import JsonResponse

import datetime, pika, time

def start(request):
    publishTime()
    return JsonResponse({"start": "starting"})

@background()
def publishTime():
    print("clock initiated")
    while True:
        now = datetime.datetime.now().strftime('%H:%M:%S')
        sendMessage(str(now))
        time.sleep(1)

def sendMessage(message):
    credentials = pika.PlainCredentials("0806444524", "0806444524")
    connectionParameters = pika.ConnectionParameters("152.118.148.95", 5672, "/0806444524", credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="1606831035_TOPIC", exchange_type="topic")
    channel.basic_publish(exchange="1606831035_TOPIC", routing_key="time", body=message)
    print(" [x] Sent %r" % (message))
    connection.close()